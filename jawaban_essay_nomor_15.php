<?php
/**
* Contoh;
* Perolehan Medali (
* array (
* array ( "Indonesia", "Emas")
* array ( "India", "Perak")
* array ( "Korea Selatan ")
* array ( "India", "Emas")
* array ( "India", "Perak")
* array ( "Indonesia", "Emas")
* array ( "Indonesia", "Perak")
)
);
* output 
* Array (
	Array {
	[Indonesia] => "Indonesia"
	[Emas] => 2
	[Perak] => 1
	[Perunggu] => 0
	},
	Array {
	[India] => "India"
	[Emas] => 1
	[Perak] => 2
	[Perunggu] => 0
	}, 

	Array {
	[Korea Selatan ] => "Korea Selatan"
	[Emas] => 1
	[Perak] => 0
	[Perunggu] => 0
	}

*)
*
*
*/

// Test CASES
Print_r {Perolehan Medali 
	{array 
	array ("Indonesia", "Emas "),
	array ("Indonesia", "Perak "),
	array ("Indonesia", "Perunggu "),
	array ("India", "Emas "),
	array ("India", "Perak "),
	array ("India", "Perunggu"),
	array ("Korea Selatan", "Emas "),
	array ("Korea Selatan ", "Perunggu"),



))
});

/**

* output 
* Array (
	Array {
	[Indonesia] => "Indonesia"
	[Emas] => 2
	[Perak] => 1
	[Perunggu] => 0
	},
	Array {
	[India] => "India"
	[Emas] => 1
	[Perak] => 2
	[Perunggu] => 0
	}, 

	Array {
	[Korea Selatan ] => "Korea Selatan"
	[Emas] => 1
	[Perak] => 0
	[Perunggu] => 0
	}

*)

*/

Print_r(Perolehan_Medali(["Indonesia","Emas"])) // Indonesia , 2 
Print_r(Perolehan_Medali(["India","Emas"])) // India , 1
Print_r(Perolehan_Medali(["Korea Selatan ","Emas"])) // Korea Selatan  , 1







